#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <PubSubClient.h>
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>

// Constants
const char* autoconf_ssid = "ESP8266_DEVICENAME";
const char* autoconf_password = "CONFIG_WIFI_PWD";
const char* mqtt_server = "10.0.1.4"; //MQTT Server IP

// MQTT Constants
const char* mqtt_devicestatus_set_topic = "homeprague/room/device_name/devicestatus";
const char* mqtt_pingallresponse_set_topic = "homeprague/pingallresponse";
const char* mqtt_relayone_set_topic = "homeprague/room/relayone/status";
const char* mqtt_relaytwo_set_topic = "homeprague/room/relaytwo/status";
const char* mqtt_relayone_get_topic = "homeprague/room/relayone";
const char* mqtt_relaytwo_get_topic = "homeprague/room/relaytwo";
const char* mqtt_pingall_get_topic = "homeprague/pingall";

// Global
byte relayone_state;
byte relaytwo_state;

WiFiClient espClient;
PubSubClient client(espClient);

void setup_ota() {

  // Set OTA Password
  ArduinoOTA.setPassword("ESP8266_PASSWORD");
  ArduinoOTA.onStart([]() {});
  ArduinoOTA.onEnd([]() {});
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {});
  ArduinoOTA.onError([](ota_error_t error) {
    if (error == OTA_AUTH_ERROR);          // Auth failed
    else if (error == OTA_BEGIN_ERROR);    // Begin failed
    else if (error == OTA_CONNECT_ERROR);  // Connect failed
    else if (error == OTA_RECEIVE_ERROR);  // Receive failed
    else if (error == OTA_END_ERROR);      // End failed
  });
  ArduinoOTA.begin();

}

void reconnect() {

  // MQTT reconnection function

  // Loop until we're reconnected
  while (!client.connected()) {

    // Create a random client ID
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);

    // Attempt to connect
    if (client.connect(clientId.c_str())) {

      // Once connected, publish an announcement...
      client.publish(mqtt_devicestatus_set_topic, "connected");
      // ... and resubscribe
      client.subscribe(mqtt_relayone_get_topic);
      client.subscribe(mqtt_relaytwo_get_topic);
      client.subscribe(mqtt_pingall_get_topic);

    } else {

      // Wait 5 seconds before retrying
      delay(5000);

    }

  }

}

void callback(char* topic, byte* payload, unsigned int length) {

    char c_payload[length];
    memcpy(c_payload, payload, length);
    c_payload[length] = '\0';

    String s_topic = String(topic);         // Topic
    String s_payload = String(c_payload);   // Message content

  // Handling incoming MQTT messages

    if ( s_topic == mqtt_relayone_get_topic ) {

      if (s_payload == "1") {

        if (relayone_state != 1) {

          // digitalWrite(Dx,HIGH);
          client.publish(mqtt_relayone_set_topic, "1");
          relayone_state = 1;

        }

      } else if (s_payload == "0") {

        if (relayone_state != 0) {

          // digitalWrite(Dx,LOW);
          client.publish(mqtt_relayone_set_topic, "0");
          relayone_state = 0;

        }

      }

    } else if ( s_topic == mqtt_relaytwo_get_topic ) {

      if (s_payload == "1") {

        if (relaytwo_state != 1) {

          // digitalWrite(Dy,HIGH);
          client.publish(mqtt_relaytwo_set_topic, "1");
          relaytwo_state = 1;

        }

      } else if (s_payload == "0") {

        if (relaytwo_state != 0) {

          // digitalWrite(Dy,LOW);
          client.publish(mqtt_relaytwo_set_topic, "0");
          relaytwo_state = 0;

        }

      }

    } else if ( s_topic == mqtt_pingall_get_topic ) {

      client.publish(mqtt_pingallresponse_set_topic, "{\"device_name\":\"connected\"}");

    }


}

void setup() {

  //Relay setup
  //pinMode(Dx,OUTPUT);
  //digitalWrite(Dx,HIGH); // Turn ON by default ca. 0,5s after power-up
  //pinMode(Dy,OUTPUT);
  //digitalWrite(Dy,HIGH); // Turn ON by default ca. 0,5s after power-up

  pinMode(BUILTIN_LED, OUTPUT);     //Initialize the BUILTIN_LED pin as an output
  Serial.begin(115200);
  WiFiManager wifiManager;
  wifiManager.autoConnect(autoconf_ssid,autoconf_password);
  setup_ota();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
  digitalWrite(BUILTIN_LED, HIGH);  //Turn off led as default, also signal that setup is over

}

void loop() {

  if (!client.connected()) {
      reconnect();
    }
    client.loop();
    ArduinoOTA.handle();

}